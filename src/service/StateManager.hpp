#ifndef STATE_MANAGER_SERVICE_HPP
#define STATE_MANAGER_SERVICE_HPP
#include "tools/Trace.hpp"

enum State
{
    COLD_BOOT,
    FULL_INITIALIZATION,
    WATCHDOG_RESET,
    DEFECT_CHECKING,
    MOVEMENT_DETECTED,
    ALARM_WAKEUP,
    IDLE,
    HIBERNATE
};

constexpr int numberOfSteps = HIBERNATE + 1;

constexpr State nextStep[numberOfSteps] = {
    FULL_INITIALIZATION,
    IDLE,
    DEFECT_CHECKING,
    COLD_BOOT,
    IDLE,
    IDLE,
    HIBERNATE,
    IDLE
};

using StateExecutor = void (*)();

void setupBasePeripherals();
void restoreBasePeripherals();
void executeFullInitialization();
void executeDefectChecking();
void executeIdle();
void executeHibernate();

constexpr StateExecutor executors[numberOfSteps] = {
    setupBasePeripherals,
    executeFullInitialization,
    restoreBasePeripherals,
    executeDefectChecking,
    restoreBasePeripherals,
    restoreBasePeripherals,
    executeIdle,
    executeHibernate
};

State checkBootState();

class StateService {
public:
    StateService() {
        currentState = checkBootState();
    }
    
    StateService(const StateService & other) = delete;
    StateService(StateService && other) = delete;
    StateService &operator=(const StateService & other) = delete;
    virtual ~StateService() { TRACE_EXECUTION }

    void execute() {
        TRACE_EXECUTION
        if (currentState >= COLD_BOOT && currentState <= HIBERNATE) {
            executeFrom(currentState);
            currentState = nextStep[currentState];
        }
    }

    void executeFrom(State state) {
        TRACE_EXECUTION_ARGS(state)
        if (state >= COLD_BOOT && state <= HIBERNATE) {
            executors[state]();
        }
    }

    State getCurrentState() {
        return currentState;
    }
private:
    State currentState;
};

extern StateService StateManager;
#endif // STATE_MANAGER_SERVICE_HPP