#ifndef DISPLAY_SERVICE_HPP
#define DISPLAY_SERVICE_HPP

#include <vector>
#include "SuspendableManager.hpp"
#include "tools/Trace.hpp"

class Display : public ASuspendable {
public:
    Display():initialized(false),ASuspendable() { TRACE_EXECUTION }
    Display(const Display & other) = delete;
    Display(Display && other) = delete;
    Display &operator=(const Display & other) = delete;
    virtual ~Display() { TRACE_EXECUTION }

    void begin();
    void end();
    void showOwner(const std::vector<const char *> & name, const char * location = nullptr);
    virtual void suspend();
    virtual void resume();
private:
    void clear();
    void print(const char * text, int x = 0, int y = 0);
private:
    bool initialized;
};

extern Display DisplayModule;
#endif // DISPLAY_SERVICE_HPP