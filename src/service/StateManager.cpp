#include "StateManager.hpp"
#include "config/Accelerometer.hpp"
#include "config/Display.hpp"
#include "config/Watchdog.hpp"
#include "service/PowerManager.hpp"
#include "service/TaskManager.hpp"
#include "service/Watchdog.hpp"

StateService StateManager;

void setupBasePeripherals() {
    TRACE_EXECUTION
    saveInitialTimestamp();
    setupWatchdog();
}

void restoreBasePeripherals() {
    TRACE_EXECUTION
    restoreInitialTimestamp();
    setupWatchdog();
}

void executeIdle() {
    TRACE_EXECUTION
    TaskManager.verifyTasks();
}

void executeDefectChecking() {
    TRACE_EXECUTION
}

void executeHibernate() {
    TRACE_EXECUTION
    PowerManager.hibernate(10, SECOND);
}

void executeFullInitialization() {
    TRACE_EXECUTION
    setupAccelerometer();
    setupDisplay();
    Log.infoln(L("Module ready"));
}