#ifndef REAL_TIME_CLOCK_SERVICE_HPP
#define REAL_TIME_CLOCK_SERVICE_HPP

#include <time.h>
#include "tools/Trace.hpp"

class RealTimeClock {
public:
    RealTimeClock():initialized(false) { TRACE_EXECUTION }
    RealTimeClock(const RealTimeClock & other) = delete;
    RealTimeClock(RealTimeClock && other) = delete;
    RealTimeClock & operator=(const RealTimeClock & other) = delete;
    virtual ~RealTimeClock() { TRACE_EXECUTION }

    void begin();
    const tm & getDateTime();
    uint32_t getMillis();
    uint32_t getMicros();

private:
    bool initialized;
};

extern RealTimeClock RealTimeClockModule;

#endif // REAL_TIME_CLOCK_SERVICE_HPP