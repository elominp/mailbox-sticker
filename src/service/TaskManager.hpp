#ifndef TASKS_SERVICE_HPP
#define TASKS_SERVICE_HPP

#include "infra/RadioModule.hpp"

class TaskService {
public:
    TaskService();
    TaskService(const TaskService & other) = delete;
    TaskService(TaskService && other) = delete;
    TaskService & operator=(const TaskService & other) = delete;
    virtual ~TaskService();

    void verifyTasks();
    void scheduleDoorOpeningNotification();
    void scheduleEndTransmission();
    void scheduleWatchdogReset();
};

extern TaskService TaskManager;

#endif // TASKS_SERVICE_HPP