#include <list>
#include "SuspendableManager.hpp"
#include "tools/Trace.hpp"

static std::list<ASuspendable *> suspendables;
SuspendableService SuspendableManager;

ASuspendable::ASuspendable() {
    TRACE_EXECUTION
    suspendables.push_back(this);
}

ASuspendable::~ASuspendable() {
    TRACE_EXECUTION
    suspendables.remove(this);
}

void SuspendableService::suspendAll() {
    for (ASuspendable * suspendable : suspendables) {
        suspendable->suspend();
    }
}

void SuspendableService::resumeAll() {
    for (ASuspendable * suspendable : suspendables) {
        suspendable->resume();
    }
}
