#ifndef WATCHDOG_SERVICE_HPP
#define WATCHDOG_SERVICE_HPP

void resetWatchdog();
bool watchdogTriggered();

#endif // WATCHDOG_SERVICE_HPP