#ifndef POWER_SERVICE_HPP
#define POWER_SERVICE_HPP

#include "tools/Time.hpp"

class PowerService {
public:
    PowerService();
    PowerService(const PowerService & other) = delete;
    PowerService(PowerService && other) = delete;
    PowerService &operator=(const PowerService & other) = delete;
    virtual ~PowerService();

    void sleep(long duration = 0, TimeUnit unit = MILLISECOND);
    void hibernate(long duration, TimeUnit unit);
};

extern PowerService PowerManager;

#endif // POWER_SERVICE_HPP