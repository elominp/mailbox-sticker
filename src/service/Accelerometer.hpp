#ifndef ACCELEROMETER_SERVICE_HPP
#define ACCELEROMETER_SERVICE_HPP

#include "service/SuspendableManager.hpp"
#include "tools/Trace.hpp"

constexpr int defaultInterruptPin = 2;

struct Position {
    int x;
    int y;
    int z;
};

class Accelerometer : public ASuspendable
{
public:
    Accelerometer(): ASuspendable() { TRACE_EXECUTION }
    Accelerometer(const Accelerometer &other) = delete;
    Accelerometer(Accelerometer &&other) = delete;
    Accelerometer &operator=(const Accelerometer &other) = delete;
    virtual ~Accelerometer() { TRACE_EXECUTION }

    void begin(int interruptPin = defaultInterruptPin);
    void end();
    virtual void suspend();
    virtual void resume();
    Position getAcceleration();

private:
    void displaySensorDetails();
    void configureSensorDetection();
    void configureSensorDetectionThreshold();
    void configureSensorInactivityThreshold();
    void mapSensorInterruptToController();

private:
    int interruptPin;
};

extern Accelerometer AccelerometerModule;
#endif // ACCELEROMETER_SERVICE_HPP