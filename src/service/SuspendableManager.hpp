#ifndef SUSPENDABLE_MANAGER_SERVICE_HPP
#define SUSPENDABLE_MANAGER_SERVICE_HPP

#include "tools/Trace.hpp"

class ASuspendable {
public:
    ASuspendable();
    ASuspendable(const ASuspendable & other) = delete; // A suspendable abstract instance is not copyable as it refers by definition to distinct instances of an object -> An inherited class copy constructor should call the standard constructor of ASuspendable
    ASuspendable(ASuspendable && other) = delete; // Not sure what should be the meaning of moving an ASuspendable object...
    ASuspendable &operator=(const ASuspendable & other) = delete; // Idem, by definition we can't copy to itself another suspendable nor sure what should be doing a move operation
    virtual ~ASuspendable();

    virtual void suspend() = 0;
    virtual void resume() = 0;
};

class SuspendableService {
public:
    SuspendableService() { TRACE_EXECUTION }
    SuspendableService(const SuspendableService & other) = delete;
    SuspendableService(SuspendableService && other) = delete;
    ~SuspendableService() { TRACE_EXECUTION }

    void suspendAll();
    void resumeAll();
};

extern SuspendableService SuspendableManager;

#endif // SUSPENDABLE_MANAGER_SERVICE_HPP