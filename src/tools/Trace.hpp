#ifndef TRACE_HPP
#define TRACE_HPP

#include <Arduino.h>
#include <ArduinoLog.h>
#include <string>
#include <list>
#include "MemoryInfo.hpp"
#include "Time.hpp"

#ifndef DISABLE_LOGGING
#define TRACE_EXECUTION           Tracer tracer(__FILE__, __PRETTY_FUNCTION__);
#define TRACE_EXECUTION_ARGS(...) Tracer tracer(__FILE__, __PRETTY_FUNCTION__, __VA_ARGS__);
#define LV(str, ...) "[ %s ] [ %s ] [ Line %d ] " str, __FILE__, __PRETTY_FUNCTION__, __LINE__, __VA_ARGS__
#define L(str) LV(str, nullptr)

class Tracer {
public:
    Tracer(const char * filename, const char * function):
        filename(filename),
        function(function),
        startingAt(milliseconds()),
        firstArgument(true)
    {
        Log.traceln("%s - %s", filename, function);
        MemoryInfo::trace();
    }

    template <typename ...Args>
    Tracer(const char * filename, const char * function, Args... functionArgs):
        filename(filename),
        function(function),
        startingAt(milliseconds()),
        firstArgument(true)
    {
        Log.trace("%s - %s(", filename, function);
        (printArgument(functionArgs), ...);
        println(")");
        MemoryInfo::trace();
    }

    virtual ~Tracer() {
        MemoryInfo::trace();
        Log.traceln("%s - %s: Executed in %d ms", filename, function, milliseconds() - startingAt);
    }

private:
    template<class T>
    void printArgument(T const & t) {
        if (firstArgument) {
            firstArgument = false;
        } else {
            print(", ");
        }
        print(t);
    }

    template<class T>
    void print(T const & t) {
        Serial.print(t);
    }

    void print(void * ptr) {
        Serial.printf("%p", ptr);
    }

    void print(const std::string & s) {
        Serial.print(s.c_str());
    }

    void print(const std::string * s) {
        Serial.print(s != nullptr ? s->c_str() : "null");
    }

    template<class U>
    void print(const std::vector<U> & v) {
        bool firstItem = true;
        print("{ ");
        for (const U & i : v) {
            if (firstItem) {
                firstItem = false;
            } else {
                print(", ");
            }
            print(i);
        }
        print(" }");
    }

    template<class T>
    void println(T const & t) {
        Serial.println(t);
    }

    const char * filename;
    const char * function;
    const long startingAt;
    bool firstArgument;
};

#else
#define TRACE_EXECUTION
#define TRACE_EXECUTION_ARGS(...)
#define LV(...)
#define L(...)
#endif

#endif // TRACE_HPP