#include <stddef.h>
#include <HardwareTimer.h>
#include "MemoryInfo.hpp"
#include "Trace.hpp"

// Names of global variables from: https://github.com/stm32duino/STM32Examples/blob/main/examples/Benchmarking/MemoryAllocationStatistics/MemoryAllocationStatistics.ino

extern "C" char *sbrk(int i);

__attribute__((optimize("O0"))) void * getStackPointer()
{
    size_t var = 0;
    return (void *)((size_t) &var + sizeof(size_t));
}

extern char _end;
extern char _sdata;
extern char _estack;
extern char _Min_Stack_Size;

static char * ramstart = &_sdata;
static char * ramend = &_estack;
static char * minSP = (char *)(ramend - &_Min_Stack_Size);

static char * heapStart = (char *) sbrk(0);
static char * heapEnd = (char *) sbrk(0);
static char * stackStart = (char *) getStackPointer();
static size_t heapSize = 0;
static size_t stackSize = 0;
static size_t freeRam = 0;

static const size_t ramSize = (size_t)(ramend - ramstart);

static HardwareTimer timer(TIM17);

void refresh() {
    heapEnd = (char *) sbrk(0);
    char * stackPointer = (char *) getStackPointer();
    heapSize = (size_t)(heapEnd - heapStart);
    stackSize = (size_t)(stackStart - stackPointer);
    freeRam = (size_t)(stackPointer - heapEnd);

    if (stackPointer <= heapEnd) {
        abort(); // Stack and heap have collided, imminent disaster
    }
}

void MemoryInfo::trace(bool refreshStatistics) {
    if (refreshStatistics) {
        refresh();
    }
    Log.traceln("RAM Size: %d", ramSize);
    Log.traceln("Heap Size: %d", heapSize);
    Log.traceln("Stack Size: %d", stackSize);
    Log.traceln("Free RAM: %d", freeRam);
}

void setupMemoryProbe() {
    TRACE_EXECUTION
#ifdef MEMORY_PROBE_REFRESH_FREQUENCY_HERTZ
    timer.setOverflow(MEMORY_PROBE_REFRESH_FREQUENCY_HERTZ, HERTZ_FORMAT);
    timer.attachInterrupt(refresh);
    timer.resume();
#endif // MEMORY_PROBE_REFRESH_FREQUENCY_HERTZ
}
