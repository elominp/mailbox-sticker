#ifndef TIME_SERVICE_HPP
#define TIME_SERVICE_HPP

#include <stdint.h>

enum TimeUnit {
    YEAR,
    SEMESTER,
    QUARTER,
    MONTH,
    WEEK,
    DAY,
    HOUR,
    MINUTE,
    SECOND,
    MILLISECOND,
    MICROSECOND
};

uint32_t milliseconds();
uint32_t toMilliseconds(long duration, TimeUnit unit);
uint32_t toMicroseconds(long duration, TimeUnit unit);
void wait(long duration, TimeUnit unit = MILLISECOND);
void saveInitialTimestamp();
void restoreInitialTimestamp();

#endif // TIME_SERVICE_HPP