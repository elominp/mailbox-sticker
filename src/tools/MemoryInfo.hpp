#ifndef MEMORY_INFO_HPP
#define MEMORY_INFO_HPP

class MemoryInfo {
public:
    static void trace(bool refreshStatistics = true);
};

#endif // MEMORY_INFO_HPP