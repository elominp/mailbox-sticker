#include "config/Logging.hpp"
#include "config/RealTimeClock.hpp"
#include "service/StateManager.hpp"

void setup() {
  setupRealTimeClock();
  setupLogging();
  Log.traceln("Initial boot state = %d", StateManager.getCurrentState());
}

void loop() {
  StateManager.execute();
}