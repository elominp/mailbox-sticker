#ifndef LOGGING_CONFIG_HPP
#define LOGGING_CONFIG_HPP

#include <Arduino.h>
#include <ArduinoLog.h>
#include <time.h>
#include "service/RealTimeClock.hpp"

void printDurationSinceBoot(Print & printable) {
  printable.printf("[ %u ] ", milliseconds());
}

void printLogLevel(Print & printable, int logLevel) {
    static const char * logLevelSuffixes[] = {
        "[ SILENT ] ",
        "[ FATAL ] ",
        "[ ERROR ] ",
        "[ WARNING ] ",
        "[ INFO ] ",
        "[ TRACE ] ",
        "[ VERBOSE ] "
    };
    printable.print(logLevelSuffixes[logLevel]);
}

void printDateTime(Print & printable) {
    static char buffer[22];
    strftime(buffer, sizeof(buffer), "%Y-%m-%dT%H:%M:%SZ", &RealTimeClockModule.getDateTime());
    printable.printf("[ %s ] ", buffer);
}

void loggerPrefix(Print * printable, int logLevel) {
    printDurationSinceBoot(*printable);
    printDateTime(*printable);
    printLogLevel(*printable, logLevel);
}

void loggerSuffix(Print * printable, int logLevel) {
    printable->flush();
}

void setupLogging() {
    Serial.begin(SERIAL_BAUDRATE);
    Log.begin(LOG_LEVEL, &Serial, false);
    Log.setPrefix(loggerPrefix);
    Log.setSuffix(loggerSuffix);
    Log.infoln("Initializing...");
}

#endif // LOGGING_CONFIG_HPP