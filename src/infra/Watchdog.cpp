#include <IWatchdog.h>
#include "service/TaskManager.hpp"
#include "tools/Trace.hpp"
#include "tools/Time.hpp"

void setupWatchdog() {
    TRACE_EXECUTION
    IWatchdog.begin(toMicroseconds(WATCHDOG_TIMEOUT, SECOND));
    IWatchdog.clearReset();
    TaskManager.scheduleWatchdogReset();
}

void resetWatchdog() {
    //TRACE_EXECUTION
    IWatchdog.reload();
}

bool watchdogTriggered() {
    //TRACE_EXECUTION
    return IWatchdog.isReset();
}