#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_ADXL345_U.h>
#include "service/Accelerometer.hpp"
#include "service/TaskManager.hpp"

constexpr long minimumDurationInMillisecondsBetweenEvents = 500;

static Adafruit_ADXL345_Unified accelerometer;
Accelerometer AccelerometerModule;

void setupAccelerometer() {
    AccelerometerModule.begin();
}

void handleAccelerometerInterrupt()
{
    static long lastInterruptTime = -1;
    long currentTime = millis();

    if (currentTime - lastInterruptTime >= minimumDurationInMillisecondsBetweenEvents)
    {
        lastInterruptTime = currentTime;
        TaskManager.scheduleDoorOpeningNotification();
    }
}

void Accelerometer::begin(int interruptPin)
{
    TRACE_EXECUTION_ARGS(interruptPin)
    this->interruptPin = interruptPin;

    pinMode(D4, OUTPUT);
    digitalWrite(D4, HIGH);

    if (!accelerometer.begin())
    {
        Log.fatalln(L("Ooops, no ADXL345 detected ... Check your wiring!"));
        wait(100);
        abort();
    }

    Log.infoln(L("ADXL345 initialized"));

    accelerometer.setRange(ADXL345_RANGE_2_G);
    accelerometer.setDataRate(ADXL345_DATARATE_3200_HZ);

    displaySensorDetails();
    configureSensorDetection();
}

void Accelerometer::end() {
    Wire.end();
    digitalWrite(D4, LOW);
    pinMode(D4, INPUT_ANALOG);
}

void Accelerometer::suspend() {
    end();
}

void Accelerometer::resume() {
    begin(interruptPin);
}

void Accelerometer::displaySensorDetails()
{
    TRACE_EXECUTION
    sensor_t sensor;
    accelerometer.getSensor(&sensor);
    Log.traceln(LV("Sensor: %s", sensor.name));
    Log.traceln(LV("Driver Ver: %d", sensor.version));
    Log.traceln(LV("Unique ID: %d", sensor.sensor_id));
    Log.traceln(LV("Max Value: %f m/s^2", sensor.max_value));
    Log.traceln(LV("Min Value: %f m/s^2", sensor.min_value));
    Log.traceln(LV("Resolution: %f m/s^2", sensor.resolution));
}

void Accelerometer::configureSensorDetection()
{
    TRACE_EXECUTION
    configureSensorDetectionThreshold();
    configureSensorDetectionThreshold();
    mapSensorInterruptToController();
}

void Accelerometer::configureSensorDetectionThreshold()
{
    TRACE_EXECUTION
    accelerometer.writeRegister(ADXL345_REG_THRESH_ACT, 0xFF);
}

void Accelerometer::configureSensorInactivityThreshold()
{
    TRACE_EXECUTION
    accelerometer.writeRegister(ADXL345_REG_ACT_INACT_CTL, 0x77);
}

void Accelerometer::mapSensorInterruptToController()
{
    TRACE_EXECUTION
    accelerometer.writeRegister(ADXL345_REG_INT_MAP, 0x10);
    accelerometer.writeRegister(ADXL345_REG_INT_ENABLE, 0x10);
    attachInterrupt(interruptPin, handleAccelerometerInterrupt, CHANGE);
}

Position Accelerometer::getAcceleration() {
    TRACE_EXECUTION
    return { accelerometer.getX(), accelerometer.getY(), accelerometer.getZ() };
}