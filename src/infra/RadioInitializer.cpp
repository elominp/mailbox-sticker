#include "RadioInitializer.hpp"

RadioInitializer RadioInitializer::instance;
STM32WLx_Module RadioInitializer::moduleRadio;
STM32WLx RadioInitializer::stm32Radio(&RadioInitializer::moduleRadio);
LoRaWANNode RadioInitializer::node(&stm32Radio, &EU868);