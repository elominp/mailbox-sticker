#include <Arduino.h>
#include "STM32RTC.h"
#include "service/RealTimeClock.hpp"
#include "service/PowerManager.hpp"
#include "tools/Time.hpp"
#include "tools/Trace.hpp"

constexpr uint32_t notInitializedFlag = 0xFFFFFFFF;

static uint32_t initialMillis = notInitializedFlag;
static uint32_t initialMicros = notInitializedFlag;

void checkInitialMillis(uint32_t currentMillis) {
    if (currentMillis <= initialMillis) {
        // rollout of the milliseconds counter
        initialMillis = currentMillis;
    }
}

void checkInitialMicros(uint32_t currentMicros) {
    if (currentMicros <= initialMicros) {
        initialMicros = currentMicros;
    }
}

void saveInitialTimestamp() {
    HAL_RTCEx_BKUPWrite(STM32RTC::getInstance().getHandle(), RTC_BKP_DR0, initialMillis);
}

void restoreInitialTimestamp() {
    initialMillis = HAL_RTCEx_BKUPRead(STM32RTC::getInstance().getHandle(), RTC_BKP_DR0);
    initialMicros = initialMillis * 1000;
}

uint32_t external_millis() {
    uint32_t currentMillis = RealTimeClockModule.getMillis();

    checkInitialMillis(currentMillis);
    return currentMillis - initialMillis;
}

uint32_t external_micros() {
    uint32_t currentMicros = RealTimeClockModule.getMicros();

    checkInitialMicros(currentMicros);
    return currentMicros - initialMicros;
}

uint32_t milliseconds() {
    return external_millis();
}

using MillisecondTimeConverter = uint32_t (*)(long duration);

uint32_t secondsToMilliseconds(long duration) {
    TRACE_EXECUTION_ARGS(duration)
    return duration * 1000;
}

uint32_t minutesToMilliseconds(long duration) {
    TRACE_EXECUTION_ARGS(duration)
    return secondsToMilliseconds(duration * 60);
}

uint32_t hoursToMilliseconds(long duration) {
    TRACE_EXECUTION_ARGS(duration)
    return minutesToMilliseconds(duration * 60);
}

uint32_t daysToMilliseconds(long duration) {
    TRACE_EXECUTION_ARGS(duration)
    return hoursToMilliseconds(duration * 24);
}

uint32_t weeksToMilliseconds(long duration) {
    TRACE_EXECUTION_ARGS(duration)
    return daysToMilliseconds(duration * 7);
}

uint32_t monthsToMilliseconds(long duration) {
    TRACE_EXECUTION_ARGS(duration)
    return daysToMilliseconds(duration * 30); // Technically incorrect, require to get current month to handle the number of days per months
}

uint32_t quartersToMilliseconds(long duration) {
    TRACE_EXECUTION_ARGS(duration)
    return monthsToMilliseconds(duration * 3); // Technically incorrect, require to get current month to compute remaining duration of the current quarter
}

uint32_t semestersToMilliseconds(long duration) {
    TRACE_EXECUTION_ARGS(duration)
    return monthsToMilliseconds(duration * 2); // Technically incorrect, require to get current month to compute remaining duration of the current semester
}

uint32_t yearsToMilliseconds(long duration) {
    TRACE_EXECUTION_ARGS(duration)
    return daysToMilliseconds(duration * 365); // Technically incorrect, require to get current year to handle leap years
}

uint32_t toMilliseconds(long duration, TimeUnit unit) {
    TRACE_EXECUTION_ARGS(duration, unit)
    MillisecondTimeConverter converters[TimeUnit::SECOND + 1] = {
        yearsToMilliseconds,
        semestersToMilliseconds,
        quartersToMilliseconds,
        monthsToMilliseconds,
        weeksToMilliseconds,
        daysToMilliseconds,
        hoursToMilliseconds,
        minutesToMilliseconds,
        secondsToMilliseconds
    };

    if (unit > MILLISECOND) {
        return 0;
    } else if (unit == MILLISECOND) {
        return duration;
    } else {
        return converters[unit](duration);
    }
}

uint32_t toMicroseconds(long duration, TimeUnit unit) {
    TRACE_EXECUTION_ARGS(duration, unit)
    if (unit > MICROSECOND) {
        return 0;
    } else if (unit == MICROSECOND) {
        return duration;
    } else  {
        return toMilliseconds(duration, unit) * 1000;
    }
}

void wait(long duration, TimeUnit unit) {
    PowerManager.sleep(duration, unit);
}