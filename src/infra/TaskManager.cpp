#include <TaskScheduler.h>
#include "tools/Trace.hpp"
#include "service/TaskManager.hpp"
#include "service/Watchdog.hpp"

TaskService TaskManager;

void helloTask() {
    TRACE_EXECUTION
    Log.traceln(L("Hello, Task!"));
}

void checkAccelerometer() {
    TRACE_EXECUTION
    Log.traceln(L("Acceleration detected!"));
}

static Scheduler scheduler;
static Task endTransmissionTask(TASK_IMMEDIATE, TASK_ONCE, []() { Radio.endTransmit(); }, &scheduler);
static Task resetWatchdogTask(TASK_IMMEDIATE, TASK_FOREVER, resetWatchdog, &scheduler);
static Task helloWorldTask(TASK_SECOND * 2, TASK_FOREVER, helloTask, &scheduler);
static Task accelerometerEventTask(TASK_IMMEDIATE, TASK_ONCE, checkAccelerometer, &scheduler);

TaskService::TaskService() {
    TRACE_EXECUTION
    scheduler.startNow();
}

TaskService::~TaskService() {
    TRACE_EXECUTION
    scheduler.disableAll();
}

void TaskService::verifyTasks() {
    //TRACE_EXECUTION
    helloWorldTask.enableIfNot();
    while (scheduler.execute()); // execute returns true during idle (no-task) runs and false when tasks are being executed...
}

void TaskService::scheduleEndTransmission() {
    TRACE_EXECUTION
    endTransmissionTask.enableIfNot();
}

void TaskService::scheduleDoorOpeningNotification() {
    TRACE_EXECUTION
    accelerometerEventTask.enableIfNot();
}

void TaskService::scheduleWatchdogReset() {
    TRACE_EXECUTION
    resetWatchdogTask.enableIfNot();
}
