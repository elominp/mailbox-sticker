#include <STM32LowPower.h>
#include "service/PowerManager.hpp"
#include "service/SuspendableManager.hpp"
#include "service/StateManager.hpp"
#include "tools/Time.hpp"
#include "tools/Trace.hpp"

PowerService PowerManager;

PowerService::PowerService() {
    TRACE_EXECUTION
    LowPower.begin();
}

PowerService::~PowerService() {
    TRACE_EXECUTION
}

void PowerService::sleep(long duration, TimeUnit unit) {
    TRACE_EXECUTION_ARGS(duration, unit);
    LowPower.sleep(toMilliseconds(duration, unit));
}

void PowerService::hibernate(long duration, TimeUnit unit) {
    TRACE_EXECUTION_ARGS(duration, unit)
    SuspendableManager.suspendAll();
    LowPower.shutdown(toMilliseconds(duration, unit));
}