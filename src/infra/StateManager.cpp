#include "stm32wle5xx.h"
#include "service/StateManager.hpp"
#include "tools/Trace.hpp"

uint32_t resetCause;

State checkBootState() {
    TRACE_EXECUTION
    resetCause = RCC->CSR;
    bool realTimeClockAlarm = resetCause & RCC_BDCR_BDRST;
    bool watchdogReset = resetCause & RCC_CSR_IWDGRSTF;
    bool resetButtonPressed = resetCause & RCC_CSR_PINRSTF;
    bool wakeUpPinStateChange = PWR->SR1 & PWR_SR1_WUF1;

    if (watchdogReset) {
        return WATCHDOG_RESET;
    } else if (resetButtonPressed) {
        return COLD_BOOT;
    } else if (wakeUpPinStateChange) {
        return MOVEMENT_DETECTED;
    } else if (realTimeClockAlarm) {
        return ALARM_WAKEUP;
    } else {
        return COLD_BOOT;
    }
}