#ifndef RADIO_MODULE_INFRA_HPP
#define RADIO_MODULE_INFRA_HPP

#include <RadioLib.h>
#include "tools/Trace.hpp"

class RadioInitializer;

class RadioModule {
public:
    friend class RadioInitializer;

    RadioModule() {
        TRACE_EXECUTION
    }
    RadioModule(const RadioModule & other) = delete;
    RadioModule(RadioModule && other) = delete;
    RadioModule & operator=(const RadioModule & other) = delete;
    virtual ~RadioModule() {
        TRACE_EXECUTION
    }

    template <typename T>
    bool startTransmit(const T & data) {
        TRACE_EXECUTION_ARGS(data)
        physicalLayer->startTransmit(data);
        return true;
    }

    bool endTransmit() {
        TRACE_EXECUTION
        physicalLayer->finishTransmit();
        return true;
    }

private:
    void setPhysicalLayer(PhysicalLayer & physicalLayer) {
        TRACE_EXECUTION_ARGS((void *) &physicalLayer)
        this->physicalLayer = &physicalLayer;
    }

private:
    PhysicalLayer * physicalLayer;
};

extern RadioModule Radio;

#endif // RADIO_MODULE_INFRA_HPP