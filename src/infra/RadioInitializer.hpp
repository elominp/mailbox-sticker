#ifndef RADIO_INITIALIZER_INFRA_HPP
#define RADIO_INITIALIZER_INFRA_HPP

#include <RadioLib.h>
#include <ArduinoLog.h>
#include "tools/Trace.hpp"
#include "RadioModule.hpp"

using RadioCallback = void (*)(void);

class RadioInitializer {
public:
    RadioInitializer(const RadioInitializer & other) = delete;
    RadioInitializer(RadioInitializer && other) = delete;
    RadioInitializer &operator=(const RadioInitializer & other) = delete;
    virtual ~RadioInitializer() {
        TRACE_EXECUTION
    }

    static RadioInitializer & begin() {
        TRACE_EXECUTION
        stm32Radio.setRfSwitchTable(rfswitchPins, rfswitchTable);
        return instance;
    }

    RadioInitializer & withRadioFrequency(float frequency) {
        TRACE_EXECUTION_ARGS(frequency)
        return verifyState(stm32Radio.setFrequency(frequency));
    }

    RadioInitializer & withTemperatureCompensatedCrystalOscillator(float voltage) {
        TRACE_EXECUTION_ARGS(voltage)
        return verifyState(stm32Radio.setTCXO(1.7));
    }

    RadioInitializer & withDataInputOutputAction(RadioCallback callback) {
        TRACE_EXECUTION_ARGS((void *) callback)
        stm32Radio.setDio1Action(callback);
        return *this;
    }

    RadioModule & build() {
        TRACE_EXECUTION
        const uint64_t joinEUI = 0xAABBCCDDEEFFAABB;
        const uint64_t deviceEUI = 0x2A2A2A2A2A2A2A2A;
        //const char * networkKey = "Hello, World!424";
        const uint8_t appKey[] = { 0xD7, 0x31, 0xD6, 0x82, 0x50, 0x30, 0x60, 0x58, 0x77, 0x79, 0xC1, 0x2B, 0x50, 0x49, 0xF8, 0x63 };
        volatile int state;
        
        state = stm32Radio.begin(868.0);
        state = stm32Radio.setTCXO(1.7);
        state = stm32Radio.setBandwidth(125.0);
        state = stm32Radio.setOutputPower(14);
        state = stm32Radio.setRxBoostedGainMode(true);
        state = stm32Radio.setSpreadingFactor(12);
        Log.traceln(LV("Channel is: %d", stm32Radio.scanChannel()));
        state = node.beginOTAA(
            joinEUI,
            deviceEUI,
            (uint8_t *) appKey,
            (uint8_t *) appKey
        );
        if (state != RADIOLIB_ERR_NONE) {
            Log.fatalln(LV("Failed to join LoRaWAN network with error: %d", state));
        }
        Log.traceln(LV("RSSI: %d dBm", (int) stm32Radio.getRSSI()));
        Log.traceln(LV("SNR: %d dB", (int) stm32Radio.getSNR()));
        Log.traceln(LV("Frequency error: %d Hz", (int) stm32Radio.getFrequencyError()));
        Radio.setPhysicalLayer(stm32Radio);
        return Radio;
    }

protected:
    RadioInitializer() {
        TRACE_EXECUTION
    }

    RadioInitializer & verifyState(int state)
    {
        TRACE_EXECUTION_ARGS(state)
        if (state != RADIOLIB_ERR_NONE) {
            Log.fatalln(LV("Initialization failed, code %d", state));
            abort();
        }
        return *this;
    }

private:
    static RadioInitializer instance;
    static STM32WLx_Module moduleRadio;
    static STM32WLx stm32Radio;
    static LoRaWANNode node;

private:
    static constexpr uint32_t rfswitchPins[] =
        {PC3, PC4, PC5};
    static constexpr Module::RfSwitchMode_t rfswitchTable[] = {
        {STM32WLx::MODE_IDLE, {LOW, LOW, LOW}},
        {STM32WLx::MODE_RX, {HIGH, HIGH, LOW}},
        //{STM32WLx::MODE_TX_LP, {HIGH, HIGH, HIGH}},
        {STM32WLx::MODE_TX_HP, {HIGH, LOW, HIGH}},
        END_OF_MODE_TABLE,
    };
};

#endif // RADIO_INITIALIZER_INFRA_HPP