#include <STM32RTC.h>
#include "service/RealTimeClock.hpp"

RealTimeClock RealTimeClockModule;
static STM32RTC & rtc = STM32RTC::getInstance();
static tm dateTime;

void RealTimeClock::begin() {
    rtc.setClockSource(STM32RTC::LSE_CLOCK);
    rtc.begin();
    initialized = true;
}

const tm & RealTimeClock::getDateTime() {
    if (initialized) {
        time_t unixTime = (time_t) rtc.getEpoch();
        dateTime = *localtime(&unixTime);
    }
    return dateTime;
}

uint32_t RealTimeClock::getMillis() {
    if (!initialized) {
        return -1;
    }
    uint32_t subSeconds = 0;
    time_t unixTime = (time_t) rtc.getEpoch(&subSeconds);
    return (unixTime * 1000) + subSeconds;
}

uint32_t RealTimeClock::getMicros() {
    if (!initialized) {
        return -1;
    }
    return getMillis() * 1000; // No real microseconds support
}

void setupRealTimeClock() {
    RealTimeClockModule.begin();
}