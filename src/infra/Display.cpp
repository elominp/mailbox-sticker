#include <Arduino.h>
#include <GxEPD2_BW.h>
#include <GxEPD2_3C.h>
#include <Fonts/FreeMonoBold9pt7b.h>
#include "service/Display.hpp"
#include "tools/Time.hpp"

static GxEPD2_BW<GxEPD2_213_GDEY0213B74, GxEPD2_213_GDEY0213B74::HEIGHT> display(GxEPD2_213_GDEY0213B74(D10, D9, D8, D7));
Display DisplayModule;

constexpr int characterWidth = 12 * TEXT_SIZE;
constexpr int characterHeight = 16 * TEXT_SIZE;
constexpr int marginLeft = characterWidth;
constexpr int marginRight = characterWidth * 2;
constexpr int marginTop = characterHeight * 2;
constexpr int screenWidth = SCREEN_ROTATION == 0 ? GxEPD2_213_GDEY0213B74::WIDTH : GxEPD2_213_GDEY0213B74::HEIGHT;
constexpr int screenHeight = SCREEN_ROTATION == 0 ? GxEPD2_213_GDEY0213B74::HEIGHT : GxEPD2_213_GDEY0213B74::WIDTH;
constexpr int marginBottom = characterHeight;

void Display::begin() {
    TRACE_EXECUTION
    Log.traceln(L("Initializing communication with display"));
    display.init(0);
    this->initialized = true;
    Log.traceln(L("Erasing content of the display"));
    clear();
    Log.traceln(L("Configuring text properties"));
    display.setTextColor(GxEPD_BLACK);
    display.setFont(&FreeMonoBold9pt7b);
    display.setTextSize(TEXT_SIZE);
    display.setRotation(SCREEN_ROTATION);
}

void Display::end() {
    TRACE_EXECUTION
    display.end();
    this->initialized = false;
}

void Display::showOwner(const std::vector<const char *> & names, const char * location) {
    TRACE_EXECUTION_ARGS(names, location)
    clear();
    for (int i = 0; i < names.size(); i++) {
        print(names[i], marginLeft, marginTop + (i * (characterHeight + 2)));
    }
    if (location != nullptr) {
        print(location, screenWidth - (strlen(location) * characterWidth + marginRight), screenHeight - marginBottom);
    }
    Log.traceln(L("Displaying new text"));
    display.display();
    wait(100);
}

void Display::suspend() {
    TRACE_EXECUTION
    end();
}

void Display::resume() {
    TRACE_EXECUTION
    begin();
}

void Display::clear() {
    TRACE_EXECUTION
    display.fillScreen(GxEPD_WHITE);
}

void Display::print(const char * text, int x, int y) {
    TRACE_EXECUTION_ARGS(text, x, y)
    if (!this->initialized) {
        begin();
    }
    Log.traceln(L("Setting cursor at the requested position"));
    display.setCursor(x, y);
    Log.traceln(L("Sending new text"));
    display.print(text);
}

void setupDisplay() {
    TRACE_EXECUTION
    DisplayModule.begin();
    DisplayModule.showOwner({ "Name1", "Name2", "...etc" }, "Apartment number");
}