# Smart Mail Tag

## Does it work?

Spoiler alert: No, unfortunately, not yet 😥

I assembled (with a bit of difficulty) a first iteration of the circuit but unfortunately for now the LORA/WIO-E5 chip is not recognized and still debugging the board / trying to upload my code to the SoC.

Once I fix this issue, the main thing I'm not confident about (and already learned that I probably made some mistakes) is the antenna part as it's the first time I make a project where I need to provide it (until now the modules I used already had built-in trace antenna or the u.fl connector and the matching parts already on them) and I'm an absolute beginner in this domain.

## Teaser

Meet Smart Mail Tag, your new essential for a smarter mailbox experience. Designed for convenience and style, it's perfect for everyone, from city dwellers to those in rural areas.

What sets Smart Mail Tag apart is its elegant e-paper screen. Say goodbye to messy handwritten tags; Smart Mail Tag displays clean, classy names and information, enhancing the look of your mailbox with its modern design.

But it's not just about looks. This device is built for efficiency. With its low power consumption, you won't be hassling with frequent battery changes. It's ideal for the environmentally conscious and the busy alike.

And distance? No problem! Smart Mail Tag uses long-range communication technology, ensuring it works seamlessly whether your mailbox is in a residential hall or a country lane. You'll get real-time notifications on your phone whenever your mailbox is accessed, keeping you in the loop no matter where you are.

With Smart Mail Tag, enjoy the blend of design, convenience, and peace of mind, knowing that an added layer of security is there if you need it. Transform your mailbox into a smart, stylish hub of your postal activities with Smart Mail Tag – the future of mail technology!

(Well yeah I know, I made ChatGPT write this pitch I'm not good enough in Marketing :D)

## Why creating this project?

Now seriously, let's drop the cringe marketing teaser 🧛‍♂️, the idea behind this project was originally very simple: I wanted to be able to log and have alerts when someone interact with my mailbox (opening or closing the door for example).

Why? Well with time progressing and for several reasons I receive more and more parcels in my mailbox instead of simple letter and I'm not always at home to receive them, especially for example when the estimated delivery time frame is between literally midnight to 10pm (sorry dude that called 4am to try to deliver something, at that time of the day my phone is on mute 😴).

Most of the time it's ok tough but in some occasion the notification aren't very accurate (even SMS arrive several hours or even sometime the next day) or the delivery status doesn't reflect the real situation like in some bad cases ... delivered in the mailbox but it's empty?

Thus come the reason why I wanted to be able to track and know when there is something in the mailbox ready to fetch.

Now, about what to object to create to do this two issues arises: How to transmit a notification when the mailbox is in the hall and my apartment is on the 4th floor or even for some of my family members when the mailbox is on the street? And where to put this logger somewhere else that inside these big nice metallic boxes? 🤔

I thought that for this logger to work it would be best to stick it outside of the mailbox on the door and by putting an e-ink screen (or even just a hand written paper for a cheaper version) on top of it it could be a nice name tag for the mailbox with the features I wanted.

It's about that time that I saw the STM32 contest on Elektor magazine and thought that it was a nice occasion to build it with this platform.

After all, at that time my thoughts were: "I just need to connect an E-Ink display, an accelerometer, send messages through a long range network and power everything with a cell coin. Easy right?"

Well, I quite underestimated the difficulty of this project and thus why I still wasn't able to make it work but I learned a lot of things and still hoping to complete it 😅

## Repository organization

This project is developed using PlatformIO so it mainly follows the usual structure with the source code in the `src` folder and the configurations in the `platformio.ini` file.

Inside the `doc` folder are some of my design / thoughts and generated documentation as well the component documentations I'm using that are stored in the `doc/assets/references` folder and which are of course owned by the companies producing these parts.

The Gerber files of the PCB are also present in the `doc/assets/gerber_files` and were created using EasyEDA from JLCPBC.

## Components

The prototype is built around these parts:

| Part Name       | PCB Label  | Footprint                        |
| --------------- | ---------- | -------------------------------- |
| LORA-E5-HF      | U1         | SMD-28_L12.0-W12.0-P1.25_LORA-E5 |
| CR2032-BS-2-1   | B1         | BAT-TH_BS-2-1                    |
| ADXL345BCCZ-RL7 | U2         | LGA-14_L5.0-W3.0-P0.80-BL        |
| U.FL-R-SMT(01)  | RF1        | ANT-SMD_UFL-R-SMT01              |
| HDR-F-2.54_1x3  | SWD        | HDR-F-2.54_1X3                   |
| HDR-F-2.54_1x4  | VCC        | HDR-F-2.54_1X4                   |
| HDR-F-2.54_2x4  | DISPLAY    | HDR-F-2.54_2X4                   |
| K2-3.6×6.1_SMD  | BOOT,RESET | KEY-SMD_2P-L6.2-W3.6-LS8.0       |
| 9.2n            | L1         | L0603                            |
| 470             | R1,R2      | R0603                            |
| 10k             | R3,R4,R5   | R0603                            |
| 100k            | R6,R7      | R0603                            |
| 4.7u            | C1         | C0603                            |
| 3.5p            | C2,C3      | C0603                            |
| 100n            | C4         | C0603                            |
| 10u             | C5         | C0603                            |
| 0.1u            | C6         | C0603                            |

Originally I used a Waveshare 2.13" E-Ink screen to prototype the code on the STM32 Nucleo but I'll adapt the code to use E-Ink screens from [WeAct Studio](https://fr.aliexpress.com/item/1005004644515880.html?spm=a2g0o.productlist.main.3.7a34TohcTohc3n&algo_pvid=c8beff9d-68d7-44f8-a0f4-e320e3acb1db&algo_exp_id=c8beff9d-68d7-44f8-a0f4-e320e3acb1db-1&pdp_npi=4%40dis%21EUR%218.90%218.90%21%21%2167.97%2167.97%21%40210385db17091577409738782e1b9d%2112000034319637608%21sea%21FR%21196477498%21&curPageLogUid=9dHxWOIvEMJW&utparam-url=scene%3Asearch%7Cquery_from%3A) that have a pinout I prefer and are available in 2.13" and 2.9" variants.

The ADXL accelerometer chip was harvested from a spare module, as I discovered later the wonder and magic of the hot plates it was super easy to get it from one of these modules and it's cheaper that ordering the sole IC in low quantities.

Just put one of these modules on a hot plate, crank up the temperature to around 250 degrees Celsius and as soon as the solder melts with the temperature increase just pick the tiny chip 😁

For the antenna I bought a couple of u.fl models to try from AliExpress.

The PCB was ordered on JLCPBC and I didn't know at that time, but I should have ordered the stencil and would advise you to not make the same mistake as me 😅

## Tools

The project was supposed to be relatively simple, so basic tools such as a soldering iron and a ST-Link (or similar programmer) should be enough.

However to work and debug on the code of this project a simple logic analyzer is welcomed as well I think for an RTL-SDR and a NanoVNA (currently learning how to use them, I said I was a beginner in radio stuff 😅).

Also, it was my first time soldering this much SMD components (until now I only tried for fun to solder few resistors or capacitors) and I must say that the hot plate helped a lot, I only applied low temperature solder paste on the board, positionned the components on it and then everything soldered in one pass when I turned up the heat.

However, as I didn't ordered the stencil with my PCBs I put too much paste on the tiny pads of the WIO-E5 even if I try to limit myself and it made lot of solder bridges between them that I had to remove by sucking the excess solder with solder braid.

For the code development itself I use PlatformIO on Visual Studio Code as compilation, upload and debug works globally even though it's less complete than STM Cube.

## Design

Although I had several issues to write the code for this project (and still also haven't finished), what I wanted was relatively simple:

Displaying the names of the mailbox owner on the display when a new configuration is set and globally have the controller sleeps until the accelerometer wakes up the MCU with an interrupt when movement is detected to eventually send a message through a LoRaWAN network (mainly I'm trying to use it with The Things Network).

As I had random crashes caused by interrupts when putting the chip in STOP mode (was OK in `SLEEP` mode but it's not really low power), I settled to save time to just save whatever few flags I need in the backup memory of the RTC and use the `SHUTDOWN` mode.

I know it's supposed to be a bad practice, but at least it was the simplest and quickest way to have the code work correctly in low power mode without conflict and basically the wake up cause is used at the start of the code to deduce whatever the code should do.

Also I use the RTC of the chip as the reference time instead of the TIMER used by Arduino built-in functions.

As the RTC is able to keep tracking time even in shutdown mode and with a better precision it was quite useful and I also made my logger use this as time reference for the messages and overloaded the configuration of the `TimeScheduler` library from Arkhipenko to use functions that provide the time from the RTC instead of Arduino.

As a side note, to also simplify the code I'm not using an RTOS (mBed included even though I know official STM32 Arduino cores use it behind the scene) as simple cooperative tasks are enough for me.

I also don't use the sleep management of the TimeScheduler library that normally wakes up every 1 second to check for new tasks and instead put manually the chip to shutdown mode for a defined amount of time once every current tasks are finished as there aren't time sensitive operation and only a few wake up cases.