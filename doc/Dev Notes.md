## ToDo List
- Buy a single channel LoRa module to test communication through a single channel trouble generator
- Add unit tests + HIL tests
- Find an emulator complete enough for this MCU for simulated runs, debug & CI -> Renode seems interesting to explore
- Add a command protocol to be able to configure the tag & emit events -> through UART & LoRa
    + Ping / Pong health test
    + Date configuration
    + Name configuration
    + Send movements events (door opening & closing)
    + Pull commands through LoRaWan network
- Replace fault logs and dumb functions returned checking by an assert based error handler able to:
    + Print & flush the cause to the logger
    + Display a readable error on the screen
    + Abort the chip
- Add a power sensor (INA219?) to be able to automatically monitor source power, current consumption (no need for high precision, being able to deduce run / sleep states and abnormal power consumptions is enough) and estimate the remaining battery duration
    + => Also would be more practical than always switching the jumper to put the ammeter or use the dedicated shield every time I want to check power consumption, not even talking about the dedicated RF and CPU pads that need to do board rework to use
- Maybe buy & try different e-paper screen size to test for example if 2.9" screen would be more suitable
- The MCU doesn't have integrated USB, need to make a programmer + embedded documentation interface (likely with a RP2040) for easy updates
- Design & fab PCB (base Nucleo is way too big for this)
- Design & fab enclosure
    + Take inspiration from the ChatGPT generated image, it's simple enough but has the parts and the outer lighting for additional visual feedback is a nice idea:
![Alt text](<assets/enclosure-idea.png>)
- Write documentation (thanks ChatGPT can help with this)

## Components
- STM32WL55 -> Core MCU + Radio -> Will probably use a WIO-E5-LE module from Seeed Studio
    - The STM32WL55 could probably be replaced alternatively by a more common MCU + SX radio
- 32KHz quartz required as the LSE clock for the RTC
- ADXL345 accelerometer
- 3V cell coin battery holder + cell coin
- 2.13" E-Ink black & white screen
- TPL5010? -> for external watchdog, not sure it's supposed to work in shutdown mode even though it triggers :/ could also enable to set longer reset intervals anyway and have a full minute sleeping window between RTC alarms or movement interrupts
- INA219? -> for later power management?

## Pain Points
- First generic ePaper display -> no output
- Trying a Waveshare ePaper display -> no output
- Buying and using a cheap logic analyzer -> no signal on additional connectors -> using Arduino headers works & screen display content :)
- LoRa -> too far from the gateway of TTN, need to build / buy one
- `Serial` TX and RX are not mapped to D0/D1 pins by default...
- Default font of the used library doesn't seem to support accents (well at least first tries failed, need to check deeper)
- Scaling of the font is not very practical... `1` size font is OK but a bit small and foont size `2` is stupidly too big for a 2.13" screen, need to find a way to use a font size more adequate (maybe find & try a different font with a bigger base size?)
- RTC alarm works when using LSI (default clock used by STM32RTC) but time information is lost during shutdown, so it requires LSE clock to be present for normal operation

## Bugs
- Multiple issues when using deep sleep:
    + Random MemFault from the location 0x00000000 -> probably dereferencing a null pointer but only occurs when using deep sleep -> works perfectly when using sleep so I suppose it's an issue either with some code not correctly reinitialized or interrupts
    + When using I2C (for the accelerometer) the static current consumption is high (0.7mA) but unoticeable with rest of the code / peripherals used and only disabling accelerometer code
    + Solutions to explore
        - Improve error handling & fix deep sleep code... (peripheral libraries? Arduino interfaces? LowPower library?)
        - Restructure main as a state machine to handle different tasks to execute depending on the cause of the wakeup (since main is not doing anything currently except basic initialization) and simply go to stupid shutdown mode (hmpf...)
    + Library doesn't support I2C wakeup => so need to rewrite low power code anyway or configure correctly the sensor to wakeup the MCU based on GPIO interrupt (even more reason dumb shutdown sleep with external + RTC alarm wakeup would be enough in this case)