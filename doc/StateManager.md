![state transitions](./assets/state-transitions.png)

## Overview
The code defines a `StateService` class, which is designed to manage state transitions within an application. The transitions are based on a predefined set of states represented by the `State` enumeration.

## Enum: `State`
Defines various states that the service can handle:
- `COLD_BOOT`
- `FULL_INITIALIZATION`
- `WATCHDOG_RESET`
- `DEFECT_CHECKING`
- `MOVEMENT_DETECTED`
- `ALARM_WAKEUP`
- `IDLE`
- `HIBERNATE`

## Array: `nextStep`
A constant array defining the next state for each current state. It maps every state to its subsequent state.

## Class: `StateService`
- **Constructors**
  - Default Constructor: Initializes a new instance of the `StateService` class. Traces execution start.
  - Copy Constructor (Deleted): Prevents copying of the service instance.
  - Move Constructor (Deleted): Prevents moving of the service instance.

- **Assignment Operator (Deleted)**
  - Prevents assignment from another `StateService` instance.

- **Destructor**
  - Traces execution end upon destruction.

- **Method: `executeFrom(State state)`**
  - Initiates execution or transition from the specified state.

## Global Instance
- **`StateManager`**: A global instance of the `StateService` class for managing states throughout the application.

## Initial State
The initial state from boot is deduced by the `checkBootState` function that verify the flags of the target MCU to deduce
if the core has booted because of a power up, reset, change of state of a wake up pin or RTC alarm.

## State Transition & Power Management
All states except `IDLE` and `HIBERNATE` transitions in a linear way.

`COLD_BOOT` state execute a full initialization of all peripherals + initial saving of state information.

`ALARM_WAKEUP` and `MOVEMENT_DETECTED` only reinitialize minimum peripherals + eventual later peripherals only on first usage since wakup.

`WATCHDOG_RESET` is used to handle cases when the MCU has suffered a reset and later eventually try to detect the cause of the error. It then continues with normal `COLD_BOOT` state.

When the `IDLE` state is reached, the Task Scheduler is prompted to execute any due task and once finished transitions to
the `HIBERNATE` state.

The `HIBERNATE` state only consists in calling the `hibernate` method of the `PowerManager` that suspends all currently running
peripherals and put the MCU in deep sleep mode (either with SRAM kept and transitioning back to `IDLE` state or doing a quick reset on wake up).

Important states values such as the boot timestamp used for time computations are kept in RTC backup registers so they survive
even when `shutdown` mode is used.

## Usage
The `StateService` class is used to manage state transitions based on a predefined sequence. It is designed as a non-copyable and non-movable singleton to ensure that state management is centralized and consistent across the application. The service provides a mechanism to start execution from any given state, following the transitions defined in the `nextStep` array. 

## Note
- Documentation page generated with ChatGPT
- The state transitions are hard-coded and linear as per the `nextStep` array. If dynamic state management is required, modifications would be needed.