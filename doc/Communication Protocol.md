## Overview
Communication is not yet implemented, but will be kept as simple as possible for usage & debugging and will be message based.

## Structure
Messages will probably be based on these simple structure:

- Packet:
  + - header: optional field containing a structure used as metadata of the message
  + - message: required field containing a message to transmit, serialized either as a string (for compression, encryption or whatever else done programmatically) or native object field (both formats should be handled)
- Header:
  + id: sequence id of the packet
  + result: present only for responses, either positive values for OK results or any zero or negative values for error codes
  + date: date of emission of the packet
- Command (used in message field):
  + name: name of the command
  + value: optional, value to transmit in the command

## Transmission mode
Commands and responses will be immediately sent in push mode by the MCU but new commands to execute will be only fetched periodically in pull mode from the network / interface where they are buffered.

## Serialisation
On UART interface structures will be serialized in JSON, for LoRaWan probably JSON and eventually a binary format such as MsgPack, Protobuf or dumb binary structure dumping if JSON is too heavy.

## Commands
For now only these commands should be enough:
- DATE: set the datetime of the RTC
- NAME: set / add a name displayed on the screen
- LOCATION: set the location of the owner of the mailbox displayed on the screen
- DOOR_MOVEMENT: emit the timestamp of when a movement has been detected with the door of the mailbox
  + DOOR_OPEN + DOOR_CLOSE -> eventual specialization of detected movements